export const validateInfoCars = (carsInfo) => {
    var errorList = {};
    console.log(carsInfo);
    if (carsInfo.plaque === null || carsInfo.plaque.length === 0 ||
        /^\s+$/.test(carsInfo.plaque))
        errorList.plaque = 'Requerido';
    if (carsInfo.price === null || carsInfo.price.length === 0 ||
        /^\s+$/.test(carsInfo.price))
        errorList.price = 'Requerido';
    else if (!/^-?(?:\d+(?:,\d*)?)$/.test(carsInfo.price))
        errorList.price = 'El valor debe ser númerico';
    if (carsInfo.status === '' || /^\s+$/.test(carsInfo.status))
        errorList.status = 'Requerido';
    if (carsInfo.color === null || carsInfo.color === "" ||
        /^\s+$/.test(carsInfo.color))
        errorList.color = 'Requerido';
    if (carsInfo.model === null || carsInfo.model.length === 0 ||
        /^\s+$/.test(carsInfo.model))
        errorList.model = 'Requerido';
    else if (parseInt(carsInfo.model) < 1940 || parseInt(carsInfo.model) > 2022)
        errorList.model = 'Debe ser mayor a 1940 y menor o igual que 2022';

    return errorList;
};