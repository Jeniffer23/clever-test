import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch, } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getColors, addDataCars, changeIsCreating } from '../../../redux/carsDuck';
import TextFieldComponent from '../../../components/TextField';
import SelectFieldComponent from '../../../components/SelectField';
import { Button, Grid, FormControlLabel, Switch, Snackbar } from '@material-ui/core';
import { validateInfoCars } from './CarsValidation';

const Form = (props) => {

    const isCreating = useSelector(store => store.car.isCreating);
    const isEditedCreated = useSelector(store => store.car.isEditedCreated);
    const colors = useSelector(store => store.car.colors);
    const carRedux = useSelector(store => store.car.car);
    const [snackOpen, setSnackOpen] = useState(false);
    const [snackMessage, setSnackMessage] = useState('');
    const [error, setError] = useState({});
    const [cars, setCars] = useState({
        plaque: '', price: '', model: '',
        color: '', status: '', active: false
    });

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getColors());
    }, [dispatch]);


    const onChange = event => {
        const { name, value } = event.target;
        setCars({ ...cars, [name]: value });
    };

    const onChangeColor = event => {
        const { name, value } = event.target;
        setCars({
            ...cars, [name]: value,
        });
    };

    const onSubmit = () => {
        const CarsError = validateInfoCars(cars);
        setError(CarsError);
        if (Object.entries(CarsError).length === 0) {
            dispatch(addDataCars(cars));
            clearInfo();
        }
    };

    useEffect(() => {
        if (isEditedCreated) {
            const message = isCreating ? 'Carro guardado satisfactoriamente!' :
                'Carro actualizado satisfactoriamente!'
            setSnackOpen(true);
            setSnackMessage(message);
        }
    }, [isEditedCreated, isCreating]);

    const handleCloseSnackbar = (e, reason) => {
        if (reason === 'clickaway') return;
        setSnackOpen(false);
    };

    useEffect(() => {
        if (!isCreating && Object.entries(carRedux).length > 0) {
            setCars(carRedux);
        }
    }, [isCreating, carRedux]);

    const clearInfo = () => {
        setCars({
            plaque: '', price: '', model: '', color: '',
            colors: '', status: '', active: false
        })
        dispatch(changeIsCreating(true));
    }

    const onSwitch = event => {
        const { name, value, checked } = event.target;
        setCars({
            ...cars, [name]:
                name === 'active' ? checked : value
        });
    };

    return (
        <div style={{ margin: 30 }}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <TextFieldComponent name='plaque'
                        placeholder='Placa'
                        valueInput={cars.plaque}
                        callback={onChange.bind(this)}
                        errorInput={error.plaque}
                        title='PLACA'
                    />
                    <TextFieldComponent name='price'
                        placeholder='Precio'
                        valueInput={cars.price}
                        callback={onChange.bind(this)}
                        errorInput={error.price}
                        title='PRECIO'
                    />
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <SelectFieldComponent name='color'
                        placeholder='Seleccione el color del carro'
                        valueInput={cars.color}
                        onChange={onChangeColor.bind(this)}
                        errorInput={error.color}
                        title='COLOR DEL VEHICULO' options={colors}
                    />
                    <div>
                        <div style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                            <input type="radio" name="status" onChange={onChange.bind(this)} value="used"
                                checked={cars.status === 'used'} />
                            <p>USADO</p>
                        </div>
                    </div>
                    <div>
                        <div style={{
                            display: 'flex',
                            alignItems: 'center',
                        }}>
                            <input type="radio" name="status" onChange={onChange.bind(this)} value="new"
                                checked={cars.status === 'new'} />
                            <p>NUEVO</p>
                        </div>
                    </div>
                    <p style={{
                        color: '#DE5555', fontWeight: 500,
                        fontSize: '12px',
                        lineHeight: '14px',
                    }}>
                        {error['status']}
                    </p>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={4}>
                    <TextFieldComponent name='model'
                        placeholder='Modelo'
                        valueInput={cars.model}
                        callback={onChange.bind(this)}
                        errorInput={error.model}
                        title='MODELO'
                    />
                    <FormControlLabel control={<Switch
                        onChange={onSwitch} checked={cars.active === true} name="active" value={cars.active} />} label="Activo" name="active" />
                </Grid>
                <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }}>
                    <Button color='primary' variant='contained' onClick={onSubmit.bind(this)}
                        style={{ alignContent: 'center !important', marginRight: isCreating ? 0 : 10 }}>
                        GUARDAR
                    </Button>
                    {!isCreating &&
                        <Button color='secondary' variant='contained' onClick={() => clearInfo()}
                            style={{ marginLeft: 10 }}>
                            CANCELAR
                        </Button>
                    }
                </Grid>
                <Snackbar open={snackOpen}
                    autoHideDuration={4000}
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                    onClose={handleCloseSnackbar}
                    message={<span>{snackMessage}</span>}
                />
            </Grid>
        </div >
    );
};

export default withRouter(Form);