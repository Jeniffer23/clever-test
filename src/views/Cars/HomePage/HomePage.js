import React from 'react';
import { useSelector } from 'react-redux';
import { Grid } from '@material-ui/core';
import Title from '../../../components/Title';
import TableList from '../Table';
import Form from '../Form';

const HomePage = () => {
    const isCreating = useSelector(store => store.car.isCreating);

    return (
        <Grid container>
            <Grid item xs={6}>
                <Title title={isCreating ? 'Nuevo carro' : 'Editar carro'} />
                <Form />
            </Grid>
            <Grid item xs={6}>
                <Title title='Lista de carros' />
                <TableList />
            </Grid>
        </Grid>
    );
};

export default HomePage;